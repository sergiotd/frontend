//Bebidas Sección 1

let seccionBebidas = document.getElementById("section1");
Bebidas();
//Entrantes Sección 2

let seccionEntrantes = document.getElementById("section2");
Entrantes();
//Platos Sección 3

let seccionPlatos = document.getElementById("section3");
Platos();
//Postres Sección 4

let seccionPostres = document.getElementById("section4");
Postres();

function Bebidas(ev) {
  fetch("https://bar-maquinarias.azurewebsites.net/categoria/1", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      for (const producto of response.productos) {
        let div = document.createElement("div");
        div.style.display = "block";
        div.style.backgroundColor = "transparent";
        let button = document.createElement("button");
        button.addEventListener("click", (ev) => {
          window.localStorage.setItem(
            "producto",
            `${producto.id},${producto.nombre}`
          );
          window.location.href = "../html/ElegirComanda.html";
        });
        button.style.height = "100%";
        button.style.width = "100%";
        button.style.backgroundColor = "transparent";
        let h3 = document.createElement("h3");
        h3.innerHTML = producto.nombre;
        let h5 = document.createElement("h5");
        h5.innerHTML = producto.precio + "€";
        button.appendChild(h3);
        button.appendChild(h5);
        div.appendChild(button);
        seccionBebidas.appendChild(div);
      }
    })
    .catch((err) => {
      console.error(err);
    });
}

function Entrantes(ev) {
  fetch("https://bar-maquinarias.azurewebsites.net/categoria/4", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      for (const producto of response.productos) {
        let div = document.createElement("div");
        div.style.backgroundColor = "transparent";
        div.style.display = "block";
        let button = document.createElement("button");
        button.addEventListener("click", (ev) => {
          window.localStorage.setItem(
            "producto",
            `${producto.id},${producto.nombre}`
          );
          window.location.href = "../html/ElegirComanda.html";
        });
        button.style.height = "100%";
        button.style.width = "100%";
        let h3 = document.createElement("h3");
        h3.innerHTML = producto.nombre;
        let h5 = document.createElement("h5");
        h5.innerHTML = producto.precio + "€";
        button.appendChild(h3);
        button.appendChild(h5);
        div.appendChild(button);
        seccionEntrantes.appendChild(div);
      }
    })
    .catch((err) => {
      console.error(err);
    });
}

function Platos(ev) {
  fetch("https://bar-maquinarias.azurewebsites.net/categoria/2", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      for (const producto of response.productos) {
        let div = document.createElement("div");
        div.style.display = "block";
        div.style.backgroundColor = "transparent";
        let button = document.createElement("button");
        button.addEventListener("click", (ev) => {
          window.localStorage.setItem(
            "producto",
            `${producto.id},${producto.nombre}`
          );
          window.location.href = "../html/ElegirComanda.html";
        });
        button.style.height = "100%";
        button.style.width = "100%";
        let h3 = document.createElement("h3");
        h3.innerHTML = producto.nombre;
        let h5 = document.createElement("h5");
        h5.innerHTML = producto.precio + "€";
        button.appendChild(h3);
        button.appendChild(h5);
        div.appendChild(button);
        seccionPlatos.appendChild(div);
      }
    })
    .catch((err) => {
      console.error(err);
    });
}

function Postres(ev) {
  fetch("https://bar-maquinarias.azurewebsites.net/categoria/3", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      for (const producto of response.productos) {
        let div = document.createElement("div");
        div.style.display = "block";
        div.style.backgroundColor = "transparent";
        let button = document.createElement("button");
        button.addEventListener("click", (ev) => {
          window.localStorage.setItem(
            "producto",
            `${producto.id},${producto.nombre}`
          );
          window.location.href = "../html/ElegirComanda.html";
        });
        button.style.height = "100%";
        button.style.width = "100%";
        let h3 = document.createElement("h3");
        h3.innerHTML = producto.nombre;
        let h5 = document.createElement("h5");
        h5.innerHTML = producto.precio + "€";
        button.appendChild(h3);
        button.appendChild(h5);
        div.appendChild(button);
        seccionPostres.appendChild(div);
      }
    })
    .catch((err) => {
      console.error(err);
    });
}
