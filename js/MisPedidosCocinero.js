//Cocinero/{idCocinero}/Comandas Mis Comandas
//Comanda Preparada Comanda/{idComanda}
let DivPedidos = document.getElementById("Pedidos");

let idCocinero = window.localStorage.getItem("IdUsuario").split(",");

fetch(
  `https://bar-maquinarias.azurewebsites.net/Cocinero/${idCocinero[0]}/Comandas`,
  {
    method: "GET",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  }
)
  .then((response) => response.json())
  .then((response) => {
    for (const Pedido of response) {
      let DivClassCard = document.createElement("div");
      DivClassCard.setAttribute("class", "card");
      let DivClassCardHeader = document.createElement("div");
      DivClassCardHeader.setAttribute("class", "card-header");
      let AClassBtn = document.createElement("a");
      //Header
      AClassBtn.innerHTML = `Comanda: ${Pedido.id} Pedido: ${Pedido.idPedido}`;
      AClassBtn.setAttribute("class", "btn");
      AClassBtn.setAttribute("data-bs-toggle", "collapse");
      AClassBtn.setAttribute("href", `#id${Pedido.id}`);

      let DivIdCollapseOne = document.createElement("div");
      DivIdCollapseOne.setAttribute("id", `id${Pedido.id}`);
      DivIdCollapseOne.setAttribute("class", "collapse ");
      DivIdCollapseOne.setAttribute("data-bs-parent", "#Pedidos");

      //Body
      let DivClassCardBody = document.createElement("div");
      DivClassCardBody.setAttribute("class", "card-body");
      //Table
      let tabla = document.createElement("table");
      tabla.style.width = "100%";
      let fila = document.createElement("tr");
      let fila2 = document.createElement("tr");
      let columna1 = document.createElement("th");
      let columna2 = document.createElement("th");
      let columna3 = document.createElement("th");
      let columna4 = document.createElement("td");
      let columna5 = document.createElement("td");
      let columna6 = document.createElement("td");
      let columna7 = document.createElement("button");
      columna7.setAttribute("class", "boton btn btn-primary");

      columna7.setAttribute("onclick", `Preparar(${Pedido.id})`);

      columna1.innerHTML = "Plato:";
      columna2.innerHTML = "Descripción: ";
      columna3.innerHTML = "Categoria: ";
      columna4.innerHTML = Pedido.idProductoNavigation.nombre;
      columna5.innerHTML = Pedido.descripcion;
      columna6.innerHTML = Pedido.idProductoNavigation.idCatNavigation.nombre;
      columna7.innerHTML = "Comanda Preparada";

      fila.appendChild(columna1);
      fila.appendChild(columna2);
      fila.appendChild(columna3);

      fila2.appendChild(columna4);
      fila2.appendChild(columna5);
      fila2.appendChild(columna6);
      fila2.appendChild(columna7);

      tabla.appendChild(fila);
      tabla.appendChild(fila2);

      //Add To Collapse
      DivPedidos.appendChild(DivClassCard);
      DivClassCard.appendChild(DivClassCardHeader);
      DivClassCardHeader.appendChild(AClassBtn);
      DivClassCard.appendChild(DivIdCollapseOne);
      DivIdCollapseOne.appendChild(DivClassCardBody);
      //Boddy
      DivClassCardBody.appendChild(tabla);
    }
  })
  .catch((err) => {
    console.error(err);
  });

function Preparar(id) {
  fetch(`https://bar-maquinarias.azurewebsites.net/Cocinero/Comanda/${id}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  })
    .then((response) => {
      window.alert("Comanda Finalizada");
      window.location.reload();
    })
    .catch((err) => {
      console.error(err);
    });
}
