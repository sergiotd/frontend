let idmesa = window.localStorage.getItem("IdMesa");

let Content = document.getElementById("Mesa");

let ChildrenComandas = null;

let ChildrenBebidas = null;

let idPedido = window.localStorage.getItem("idPedido");

crear();

function crear() {
  fetch("https://bar-maquinarias.azurewebsites.net/Mesa", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((Response) => Response.json())
    .then((response) => {
      for (const mesa of response) {
        if (mesa.location == "Interior") {
          let divo = document.createElement("div");
          divo.style.justifyContent = "center";
          let img = document.createElement("img");
          img.setAttribute("src", "../assets/mesa.png");
          img.setAttribute(
            "onclick",
            `redirect('../html/Mesa.html',${mesa.id})`
          );
          img.setAttribute("id", mesa.id);
          img.style.justifyContent = "center";

          let p = document.createElement("p");
          p.innerHTML = `<b>${mesa.id}</b>`;
          divo.appendChild(img);
          divo.appendChild(p);
          Content.appendChild(divo);
        }
      }
      console.log(response);
    })
    .catch((err) => {
      console.error(err);
    });
}
