//Show Users
fetch("https://bar-maquinarias.azurewebsites.net/Usuario", {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
  },
})
  .then((response) => response.json())
  .then((response) => {
    var trHTML = "";
    for (const usuario of response) {
      trHTML += "<tr>";
      trHTML += `<td> </td>`;
      trHTML += `<td> ${usuario.id} </td>`;
      trHTML += `<td> ${usuario.nombre} </td>`;
      trHTML += `<td> ${usuario.apellidos} </td>`;
      trHTML += `<td> ${usuario.nss}</td>`;
      trHTML += `<td> ${usuario.username}</td>`;
      trHTML += `<td> ${usuario.rol}</td>`;
      trHTML += `<td><button type="button" class="btn btn-outline-secondary" onclick="showUserEditBox(${usuario.id})">Edit</button>`;
      trHTML += `<button type="button" class="btn btn-outline-danger" onclick="userDelete(${usuario.id}, '${usuario.rol}')">Del</button></td>`;
      trHTML += "</tr>";
    }
    document.getElementById("mytable").innerHTML = trHTML;
  })
  .catch((err) => {
    console.error(err);
  });

//Create User
function showUserCreateBox() {
  Swal.fire({
    title: "Añadir Producto",
    html:
      '<input id="id" type="hidden">' +
      '<input id="Nombre" class="swal2-input" placeholder="Nombre...">' +
      '<input id="Apellidos" class="swal2-input" placeholder="Apellidos...">' +
      '<input id="Nss" class="swal2-input" placeholder="Nss...">' +
      '<input id="Username" class="swal2-input" placeholder="Username...">' +
      '<input id="Password" class="swal2-input" type="password"  placeholder="Password...">' +
      `<select id="Rol" class="swal2-input"  placeholder="Rol...">>
        <option value="gerente">Gerente</option>
        <option value="cocinero">Cocinero</option>
        <option value="camarero">Camarero</option>
      <\select>`,
    focusConfirm: false,
    preConfirm: () => {
      userCreate();
    },
  });
}

function userCreate() {
  const Nombre = document.getElementById("Nombre").value;
  const Apellidos = document.getElementById("Apellidos").value;
  const Nss = document.getElementById("Nss").value;
  const Username = document.getElementById("Username").value;
  const Password = document.getElementById("Password").value;
  const Rol = document.getElementById("Rol").value;

  let NuevoUsuario = {
    id: 0,
    nombre: Nombre,
    apellidos: Apellidos,
    nss: Nss,
    username: Username,
    rol: Rol,
    password: Password,
  };
  console.log(NuevoUsuario);

  if (Rol == "camarero") {
    fetch("https://bar-maquinarias.azurewebsites.net/Camarero", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
      body: JSON.stringify(NuevoUsuario),
    }).then((response) => {
      console.log(response);
    });
  } else if (Rol == "cocinero") {
    fetch("https://bar-maquinarias.azurewebsites.net/Cocinero", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
      body: JSON.stringify(NuevoUsuario),
    }).then((response) => {
      console.log(response);
    });
  } else {
    fetch("https://bar-maquinarias.azurewebsites.net/Gerente", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
      body: JSON.stringify(NuevoUsuario),
    }).then((response) => {
      window.location.reload();
    });
  }
}

//Edit a user
function showUserEditBox(id) {
  fetch(`https://bar-maquinarias.azurewebsites.net/Usuario/${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      let gerenteSelected = "";
      let camareroSelected = "";
      let cocineroSelected = "";

      console.log(response.rol);
      if (response.rol == "gerente") {
        gerenteSelected = "selected";
      }else if(response.rol == "cocinero"){
        cocineroSelected = "selected";
      }else{
       camareroSelected = "selected" 
      }
      Swal.fire({
        title: "Editar Usuario",
        html:
          `<input id="id" type="hidden" value=${response.id}>` +
          `<input id="nombre"  class="swal2-input" placeholder="Nombre..." value="${response.nombre}">` +
          `<input id="apellidos"  class="swal2-input" placeholder="Apellidos..." value="${response.apellidos}">` +
          `<input id="nss"  class="swal2-input" placeholder="Nss..." value="${response.nss}">` +
          `<input id="username"  class="swal2-input" placeholder="Username..." value="${response.username}">` +
          `<input id="password"  class="swal2-input"  placeholder="Password..." value="" required>` +
          `<select id="Rol" class="swal2-input">
              <option ${gerenteSelected} value="gerente">Gerente</option>
              <option ${cocineroSelected} value="cocinero">Cocinero</option>
              <option ${camareroSelected} value="camarero">Camarero</option>
            <\select>`,

        focusConfirm: false,
        preConfirm: () => {
          userEdit();
        },
      });
    })
    .catch((err) => {
      console.error(err);
    });
}

function userEdit(params) {
  const id = document.getElementById("id").value;
  const nombre = document.getElementById("nombre").value;
  const apellidos = document.getElementById("apellidos").value;
  const nss = document.getElementById("nss").value;
  const username = document.getElementById("username").value;
  let pass = "";
  if(document.getElementById("password").value == ""){
    console.log(document.getElementById("password"));
     pass = null;
  }else{
     pass = document.getElementById("password").value;
  }
  
  console.log(pass);
  const rol = document.getElementById("Rol").value;

  let UsuarioEditato = {
    id: id,
    nombre: nombre,
    apellidos: apellidos,
    nss: nss,
    username: username,
    password: pass,
    rol: rol,
  };
  console.log(UsuarioEditato);
  fetch(`https://bar-maquinarias.azurewebsites.net/Usuario/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
    body: JSON.stringify(UsuarioEditato),
  }).then((response) => {
    window.location.reload();
  });
}

//Delete a Product

function userDelete(id, roleito) {
  console.log(roleito);
  fetch(`https://bar-maquinarias.azurewebsites.net/Usuario/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      window.location.reload();
    });
}
