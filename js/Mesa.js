let idmesa = window.localStorage.getItem("IdMesa");

let mesa = document.getElementById("Mesa");

let ChildrenComandas = null;

let ChildrenBebidas = null;

let idPedido = window.localStorage.getItem("idPedido");

prueba();

function borrarhijos(nodo) {
  if (nodo.hasChildNodes()) {
    while (nodo.childNodes.length >= 1) {
      nodo.removeChild(nodo.firstChild);
    }
  }
}
function borrardiv(ev) {
  let nodo = ev.currentTarget;
  let padre = nodo.parentNode;
  let primerhijo = padre.children[1];
  let arr = primerhijo.textContent.split(" ");
  let id = arr[arr.length - 1];
  console.log(id);

  fetch(`https://bar-maquinarias.azurewebsites.net/comanda/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  })
    .then((response) => {
      console.log(response);
      window.alert("Eliminado");
      borrarhijos(padre);

      ChildrenComandas.contentWindow.location.reload(true);
      ChildrenBebidas.contentWindow.location.reload(true);
    })
    .catch((err) => {
      console.error(err);
    });
}

function prueba() {
  fetch(
    `https://bar-maquinarias.azurewebsites.net/Mesa/MesaPedido/${idmesa}`,
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    }
  )
    .then((Response) => Response.json())
    .then((response) => {
      //Todo Iniciar Pedido
      if (typeof response.value !== "object") {
        borrarhijos(mesa);
        let div = document.createElement("div");
        div.style.width = "100%";
        div.style.height = "100%";
        div.style.justifyContent = "center";
        div.style.textAlign = "center";
        let parrafo = document.createElement("button");
        parrafo.setAttribute("class", "btn bg-dark");
        parrafo.style.marginTop = "25%";
        parrafo.style.color = "white";
        parrafo.style.height = "200px";
        parrafo.style.width = "100%";
        parrafo.innerHTML = "Iniciar Pedido";
        parrafo.addEventListener("click", IniciarPedido);
        div.appendChild(parrafo);
        mesa.appendChild(div);
      } else {
        borrarhijos(mesa);

        window.localStorage.setItem("idPedido", response.value.id);

        let botonCobrar = document.createElement("button");
        botonCobrar.innerHTML = "Cobrar";
        botonCobrar.style.color = "white";
        botonCobrar.style.height = "60px";
        botonCobrar.style.width = "25%";
        botonCobrar.setAttribute("class", "btn bg-dark");

        botonCobrar.addEventListener("click", Cobrar);
        let botonCuenta = document.createElement("button");
        botonCuenta.innerHTML = "Cuenta";
        botonCuenta.style.color = "white";
        botonCuenta.style.height = "60px";
        botonCuenta.style.marginLeft = "10px";
        botonCuenta.style.width = "25%";
        botonCuenta.setAttribute("class", "btn bg-dark");
        botonCuenta.addEventListener("click", Cuenta);

        let botonNuevaComanda = document.createElement("button");
        botonNuevaComanda.innerHTML = "Nueva Comanda";
        botonNuevaComanda.style.color = "white";
        botonNuevaComanda.style.height = "60px";
        botonNuevaComanda.style.marginLeft = "10px";
        botonNuevaComanda.style.width = "25%";
        botonNuevaComanda.setAttribute("class", "btn bg-dark");
        botonNuevaComanda.addEventListener("click", (ev) => {
          window.location.href = "../html/ElegirComanda.html";
        });

        let div2 = document.createElement("div");
        div2.setAttribute("class", "d-flex justify-content-center");

        div2.appendChild(botonCobrar);
        div2.appendChild(botonCuenta);
        div2.appendChild(botonNuevaComanda);

        mesa.appendChild(div2);
        mesa.appendChild(document.createElement("hr"));
        let comandas = response.value.comanda;
        for (const comanda of comandas) {
          let div = document.createElement("div");
          div.setAttribute("class", "bg-dark mb-3");

          let BotonBorrar = document.createElement("button");
          BotonBorrar.setAttribute("class", "btn btn-white");
          BotonBorrar.style.float = "right";
          BotonBorrar.addEventListener("click", borrardiv);
          BotonBorrar.style.width = "50px";
          BotonBorrar.style.height = "50px";
          BotonBorrar.innerHTML = `<img src="../assets/eliminar.png"/>`;
          div.appendChild(BotonBorrar);

          let Parent = window.parent;
          ChildrenComandas = Parent.document.getElementById("Izquierda");
          ChildrenBebidas = Parent.document.getElementById("Derecha");

          let parrafocomanda = document.createElement("p");
          parrafocomanda.innerHTML = `Comanda: ${comanda.id}`;
          parrafocomanda.style.color = "white";
          let tabla = document.createElement("table");
          tabla.style.width = "100%";
          tabla.style.color = "white";
          let fila = document.createElement("tr");
          let fila2 = document.createElement("tr");
          let columna1 = document.createElement("th");
          columna1.style.width = "25%";
          let columna2 = document.createElement("th");
          columna2.style.width = "25%";
          let columna3 = document.createElement("th");
          columna3.style.width = "25%";
          let columnaboton = document.createElement("th");
          columnaboton.style.width = "25%";
          let columna4 = document.createElement("td");
          let columna5 = document.createElement("td");
          let columna6 = document.createElement("td");
          let columnaboton2 = document.createElement("td");

          let boton = document.createElement("button");
          boton.setAttribute("class", "btn btn-primary");
          boton.innerHTML = `<img src="../assets/aceptar.png"/>`;
          boton.setAttribute("onclick", `entregar(${comanda.id})`);

          columna1.innerHTML = "Producto:";
          columna2.innerHTML = "Precio: ";
          columna3.innerHTML = "Estado: ";
          columna4.innerHTML = comanda.idProductoNavigation.nombre;
          columna5.innerHTML = comanda.idProductoNavigation.precio;
          columna6.innerHTML = comanda.estado;
          columnaboton2.appendChild(boton);

          fila.appendChild(columna1);
          fila.appendChild(columna2);
          fila.appendChild(columna3);
          fila.appendChild(columnaboton);
          fila2.appendChild(columna4);
          fila2.appendChild(columna5);
          fila2.appendChild(columna6);
          fila2.appendChild(columnaboton2);

          tabla.appendChild(fila);
          tabla.appendChild(fila2);

          div.appendChild(parrafocomanda);
          div.appendChild(tabla);

          mesa.appendChild(div);
        }
      }
    })
    .catch((err) => {
      console.error(err);
    });
}
function Cuenta(params) {
  fetch(
    `https://bar-maquinarias.azurewebsites.net/${idPedido}/cuenta`,
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    }
  )
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      alert(`Total ${response.precioTotal}€`);
    })
    .catch((err) => {
      console.error(err);
    });
}
function Cobrar(params) {
  fetch(
    `https://bar-maquinarias.azurewebsites.net/Camarero/Pedido/${idPedido}`,
    {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    }
  )
    .then((response) => {
      console.log(response);
      window.location.reload();
    })
    .catch((err) => {
      console.error(err);
    });
}

function IniciarPedido(ev) {
  let idcamarero = window.localStorage.getItem("IdUsuario").split(",");

  let pedido = {
    id: 0,
    Fecha: new Date(),
    PrecioTotal: 0,
    Estado: "0",
    IdMesa: idmesa,
    idCamarero: parseInt(idcamarero[0]),
  };
  fetch("https://bar-maquinarias.azurewebsites.net/pedido", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
    body: JSON.stringify(pedido),
  })
    .then((response) => {
      prueba();
    })
    .catch((err) => {
      console.error(err);
    });
}

function entregar(id) {
  fetch(
    `https://bar-maquinarias.azurewebsites.net/Camarero/Comanda/${id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    }
  )
    .then((response) => {
      console.log(response);
      window.alert("Comanda Entregada");
      window.location.reload();
    })
    .catch((err) => {
      console.error(err);
    });
}
