let DivDatosCocinero = document.getElementById("DatosUsuario");

let idUsuario = window.localStorage.getItem("IdUsuario").split(",");

let BotonLogout = document.getElementById("BotontLogout");

fetch(
  `https://bar-maquinarias.azurewebsites.net/Camarero/${idUsuario[0]}`,
  {
    method: "GET",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  }
)
  .then((response) => response.json())
  .then((response) => {
    let LiNombre = document.createElement("li");
    LiNombre.innerHTML = `<b>Nombre:</b> ${response.nombre}`;
    LiNombre.style.listStyle = "none";
    LiNombre.style.color = "white";
    LiNombre.style.backgroundColor = "#eeeee4";
    LiNombre.setAttribute("class", "m-5 p-2 rounded border-white ");
    LiNombre.style.textShadow = "2px 2px 5px Black";
    LiNombre.style.backgroundImage = "url(../assets/Asies.png)";

    let LiApellidos = document.createElement("li");
    LiApellidos.innerHTML = `<b>Apellido:</b> ${response.apellidos}`;
    LiApellidos.style.listStyle = "none";
    LiApellidos.style.color = "white";
    LiApellidos.style.backgroundColor = "#eeeee4";
    LiApellidos.setAttribute("class", "m-5 p-2 rounded border-white");
    LiApellidos.style.textShadow = "2px 2px 5px Black";
    LiApellidos.style.backgroundImage = "url(../assets/Asies.png)";

    let LiNss = document.createElement("li");
    LiNss.innerHTML = `<b>NSS:</b> ${response.nss}`;
    LiNss.style.listStyle = "none";
    LiNss.style.color = "white";
    LiNss.style.backgroundColor = "#eeeee4";
    LiNss.setAttribute("class", "m-5 p-2 rounded border-white");
    LiNss.style.textShadow = "2px 2px 5px Black";
    LiNss.style.backgroundImage = "url(../assets/Asies.png)";

    let LiUsername = document.createElement("li");
    LiUsername.innerHTML = `<b>User:</b> ${response.username}`;
    LiUsername.style.listStyle = "none";
    LiUsername.style.color = "white";
    LiUsername.style.backgroundColor = "#eeeee4";
    LiUsername.setAttribute("class", "m-5 p-2 rounded border-white");
    LiUsername.style.textShadow = "2px 2px 5px Black";
    LiUsername.style.backgroundImage = "url(../assets/Asies.png)";

    let LiRol = document.createElement("li");
    LiRol.innerHTML = `<b>Rol:</b> ${response.rol}`;
    LiRol.style.listStyle = "none";
    LiRol.style.color = "white";
    LiRol.style.backgroundColor = "#eeeee4";
    LiRol.setAttribute("class", "m-5 p-2 rounded border-white");
    LiRol.style.textShadow = "2px 2px 5px Black";
    LiRol.style.backgroundImage = "url(../assets/Asies.png)";

    DivDatosCocinero.appendChild(LiNombre);
    DivDatosCocinero.appendChild(LiApellidos);
    DivDatosCocinero.appendChild(LiNss);
    DivDatosCocinero.appendChild(LiUsername);
    DivDatosCocinero.appendChild(LiRol);
  })
  .catch((err) => {

  });

BotonLogout.addEventListener("click", logout);

function logout() {
  window.location.href = "../index.html";
  window.localStorage.removeItem("token");
}
