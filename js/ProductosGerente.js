//Show Products
fetch("https://bar-maquinarias.azurewebsites.net/producto", {
  method: "GET",
  headers: {
    Authorization: `Bearer ${window.localStorage.getItem("token")}`,
  },
})
  .then((response) => response.json())
  .then((response) => {
    var trHTML = "";
    console.log(response)
    for (const productos of response) {
      trHTML += "<tr>";
      trHTML += `<td> </td>`;
      trHTML += `<td> ${productos.id} </td>`;
      trHTML += `<td> ${productos.nombre} </td>`;
      trHTML += `<td> ${productos.idCatNavigation.nombre} </td>`;
      trHTML += `<td> ${productos.precio}€</td>`;
      trHTML += `<td><button type="button" class="btn btn-outline-secondary" onclick="showProductEditBox(${productos.id})">Edit</button>`;
      trHTML += `<button type="button" class="btn btn-outline-danger" onclick="productDelete(${productos.id})">Del</button></td>`;
      trHTML += "</tr>";
    }
    document.getElementById("mytable").innerHTML = trHTML;
  })
  .catch((err) => {
    console.error(err);
  });

//Create Product
function showProductCreateBox() {
  Swal.fire({
    title: "Añadir Producto",
    html:
    `<input id="Id" type="hidden">` +
    `<input id="Nombre"  class="swal2-input" placeholder="Producto..."}">` +
    `<select id="Categoria" class="swal2-input">
    <option value="1">Bebidas</option>
    <option value="2">Comida</option>
    <option value="3">Postres</option>
    <option value="4">Entrantes</option>
  <\select>` +
    `<input id="Precio"  class="swal2-input" placeholder="0.00€"">`,
    focusConfirm: false,
    preConfirm: () => {
      productCreate();
    },
  });
}

function productCreate() {
  const Nombre = document.getElementById("Nombre").value;
  const Categoria = document.getElementById("Categoria").value;
  const Precio = document.getElementById("Precio").value;

  let NuevoProducto = {
    id: 0,
    nombre: Nombre,
    idCat: parseInt(Categoria),
    precio: parseFloat(Precio),
  };

  fetch("https://bar-maquinarias.azurewebsites.net/producto", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
    body: JSON.stringify(NuevoProducto),
  })
    
    .then((response) => {
      window.location.reload();
    });
}

//Edit a product

function showProductEditBox(id) {
  fetch(`https://bar-maquinarias.azurewebsites.net/producto/${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  })
    .then((response) => response.json())
    .then((response) => {
      let bebidaSelected = "";
      let comidaSelected = "";
      let postreSelected = "";
      let entranteSelected = "";

      if(response.idCat == 1){
        bebidaSelected = "selected";
      }else if(response.idCat == 2){
        comidaSelected = "selected";
      }else if(response.idCat == 3){
        postreSelected = "selected";
      }else {
        entranteSelected = "selected";
      }
      
      Swal.fire({
        title: "Editar Producto",
        html:
          `<input id="id" type="hidden" value=${response.id}>` +
          `<input id="nombre"  class="swal2-input" placeholder="Producto..." value="${response.nombre}">` +
          `<select id="categoria" class="swal2-input">
          <option ${bebidaSelected} value="1">Bebidas</option>
          <option ${comidaSelected} value="2">Comida</option>
          <option ${postreSelected} value="3">Postres</option>
          <option ${entranteSelected} value="4">Entrantes</option>
        <\select>` +
          `<input id="precio"  class="swal2-input" placeholder="0.00€" value="${response.precio}">`,
        focusConfirm: false,
        preConfirm: () => {
          productEdit();
        },
      });
    })
    .catch((err) => {
      console.error(err);
    });
}

function productEdit(params) {
  const id = document.getElementById("id").value;
  const nombre = document.getElementById("nombre").value;
  const categoria = document.getElementById("categoria").value;
  const precio = document.getElementById("precio").value;

  let ProductoEditato = {
    id: parseInt(id),
    nombre: nombre,
    idCat: parseInt(categoria),
    precio: parseFloat(precio),
  };
  console.log(ProductoEditato);
  fetch(`https://bar-maquinarias.azurewebsites.net/producto/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
    body: JSON.stringify(ProductoEditato),
  })
    
    .then((response) => {
      Swal.fire(response);
      window.location.reload();
    });
}

//Delete a Product

function productDelete(id) {
  fetch(`https://bar-maquinarias.azurewebsites.net/producto/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  })
    .then((response) => {
      Swal.fire(response);
      window.location.reload();
    });
}
