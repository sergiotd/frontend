let productos = null;

let idPedido = window.localStorage.getItem("idPedido");

let IdUsuario = window.localStorage.getItem("IdUsuario");

let NombreProducto = document.getElementById("NombreProducto");

let IdProducto = document.getElementById("IdProducto");

let elegirProductos = document.getElementById("ElegirProducto");

let BotonAceptar = document.getElementById("BotonAceptar");
let ChildrenComandas = null;

let ChildrenBebidas = null;

ComprobarProductos();

BotonAceptar.addEventListener("click", addComanda);

function flecha(params) {
  window.location.href = "../html/Mesa.html";
}

function cargar(params) {
  let ImagenFlecha = document.getElementById("ImagenFlecha");
  let BotonCancelar = document.getElementById("BotonCancelar");
  BotonCancelar.addEventListener("click", flecha);
  ImagenFlecha.addEventListener("click", flecha);
}

function addComanda(params) {
  let comanda = {
    id: 0,
    idCamarero: parseInt(window.localStorage.getItem("IdUsuario")),
    idCocinero: null,
    idPedido: parseInt(window.localStorage.getItem("idPedido")),
    IdProducto: productos[0],
    Descripcion: null,
    Estado: "0",
  };

  let Detalles = document.getElementById("Detalles");

  comanda.Detalles = Detalles.value;

  fetch("https://bar-maquinarias.azurewebsites.net/comanda", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify(comanda),
  })
    .then((response) => {
      let Parent = window.parent;

      ChildrenComandas = Parent.document.getElementById("Izquierda");
      ChildrenComandas.contentWindow.location.reload(true);

      ChildrenBebidas = Parent.document.getElementById("Derecha");
      ChildrenBebidas.contentWindow.location.reload(true);

      flecha();
    })
    .catch((err) => {});
}

function cancelar(params) {
  window.location.href = "../html/barra.html";
  window.localStorage.removeItem("producto");
}

function ComprobarProductos(params) {
  if (window.localStorage.getItem("producto") != null) {
    productos = window.localStorage.getItem("producto").split(",");

    NombreProducto.innerHTML = `${productos[1]}`;

    IdProducto.innerHTML = `${productos[0]}`;
  }
}
