let boton = document.getElementById("boton");

let InputUser = document.getElementById("email");

let InputPass = document.getElementById("pwd");

function login() {
  let user = {
    username: InputUser.value,
    password: InputPass.value,
  };
  let myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: JSON.stringify(user),
  };

  let username = {
    username: InputUser.value,
  };

  fetch(
    "https://bar-maquinarias.azurewebsites.net/Login",
    requestOptions
  )
    .then((Response) => Response.json())
    .then((Response) => {
      console.log(Response);
      if (Response != null) {
        localStorage.setItem("token", Response.value);
        fetch(
          "https://bar-maquinarias.azurewebsites.net/Login/UsuarioLogueado",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(username),
          }
        )
          .then((response) => response.json())
          .then((response) => {
            console.log(response);
            window.localStorage.setItem(
              "IdUsuario",
              `${response.id},${response.rol}`
            );
            if (response.rol == "camarero") {
              window.location.replace("../html/dashboard.html");
            } else if (response.rol == "cocinero") {
              window.location.replace("../html/Cocinero.html");
            } else if (response.rol == "gerente") {
              window.location.replace("../html/Gerente.html");
            }
          })
          .catch((err) => {
            console.error(err);
          });
      }
    })
    .catch((error) => window.alert("Credenciales incorrectas"));
}
boton.addEventListener("click", login);
