let ListaBebidas = document.getElementById("Bebidas");

let idCamarero = window.localStorage.getItem("IdUsuario");

let numero = idCamarero[0];

fetch(
  `https://bar-maquinarias.azurewebsites.net/Camarero/Bebidas/${numero}`,
  {
    method: "GET",
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
    },
  }
)
  .then((response) => response.json())
  .then((response) => {
    for (const bebida of response) {
      let div = document.createElement("div");
      div.setAttribute("class", "rounded mb-3 rounded");
      div.style.color = "black";
      div.style.background = "rgba(255, 255, 255, .8)";
      div.style.border = "2px solid";
      let ParrafoComanda = document.createElement("p");
      ParrafoComanda.innerHTML = `Comanda: ${bebida.id}`;

      let tabla = document.createElement("table");
      tabla.style.width = "100%";

      let fila = document.createElement("tr");
      let fila2 = document.createElement("tr");

      let columna1 = document.createElement("th");

      let columna2 = document.createElement("th");
      let columna3 = document.createElement("th");
      let columna4 = document.createElement("td");
      let columna5 = document.createElement("td");
      let columna6 = document.createElement("td");

      columna1.innerHTML = "Bebida: ";

      columna2.innerHTML = "Mesa: ";

      columna3.innerHTML = "Estado: ";

      columna4.innerHTML = bebida.idProductoNavigation.nombre;
      columna5.innerHTML = bebida.idPedidoNavigation.idMesa;
      columna6.innerHTML = bebida.estado;

      fila.appendChild(columna1);
      fila.appendChild(columna2);
      fila.appendChild(columna3);

      fila2.appendChild(columna4);
      fila2.appendChild(columna5);
      fila2.appendChild(columna6);

      tabla.appendChild(fila);
      tabla.appendChild(fila2);

      div.appendChild(ParrafoComanda);
      div.appendChild(tabla);
      ListaBebidas.appendChild(div);
    }
  })
  .catch((err) => {
  });

setInterval("location.reload()", 30000);
